package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class DeleteAccountSpec extends Specification {

    static BankOperation bank;

    def setupSpec() {
        bank = new Bank();
    }

    @Unroll
    def "should delete account number #accountNumber for #user"() {

        when: "the saldo"
            def saldo = bank.deleteAccount(accountNumber)
            print(saldo)
        then: "check account existence"
            saldo != Bank.ACCOUNT_NOT_EXISTS

        where:
            user   | accountNumber  | accountBalance
            'John' | 1              | 1500
            'Tom'  | 2              | 2200
            'Mike' | 3              | 3750
            'Todd' | 4              | 123
    }
}
